import React, { useState } from 'react'
import styled from 'styled-components'
import { Form, Input, Button, Upload, message } from 'antd'
import { UploadOutlined } from '@ant-design/icons'
import PropTypes from 'prop-types'

import { getImages } from '../../axios'
import { isValidJSON } from '../../utils'

const StyledContainer = styled.div`
  display: flex;
  justify-content: flex-end;
  width: 100%;
  padding-bottom: 25px;
  border-bottom: 1px solid rgba(0, 0, 0, 0.1);
  .ant-form {
    width: fit-content;
  }
  .ant-form-item-explain {
    max-width: 220px;
    white-space: break-spaces;
  }
  .ant-row.ant-form-item:first-child {
    margin-left: 20px;
    flex: 1 1;
  }
`
const StyledUpload = styled(Upload)`
  && {
    cursor: pointer;
    &:hover {
      opacity: 0.7;
    }
  }
`

const ImageLoader = ({ onDataLoaded }) => {
  const [form] = Form.useForm()
  const [dataLoading, setLoading] = useState(false)

  const onLoadedFile = (data) => {
    isValidJSON(data)
      ? onDataLoaded(JSON.parse(data))
      : message.error('Невалидный JSON файл')
  }

  const onLoad = ({ url }) => {
    setLoading(true)
    getImages(
      url.indexOf('http://') !== -1 || url.indexOf('https://') !== -1
        ? url
        : `http://${url}`
    )
      .then((response) => {
        if (response.headers['content-type'] === 'application/json') {
          onDataLoaded(response.data)
          message.success('Картинки успешно загружены')
        } else if (response.headers['content-type'].indexOf('image/') !== -1) {
          const image = new Image()
          image.src = url
          image.onload = () => {
            console.log(image.width)
            console.log(image.height)
            onDataLoaded({
              galleryImages: [
                {
                  url: image.src,
                  width: image.width,
                  height: image.height,
                },
              ],
            })
          }
        }
      })
      .catch(() => message.error('При запросе произошла ошибка'))
      .finally(() => setLoading(false))
  }
  const uploaderProps = {
    accept: 'application/json,image/png,image/jpeg,image/jpg',
    method: 'get',
    showUploadList: false,
    transformFile: (e) => {
      console.log(e)
      if (e.type === 'application/json') {
        e.text()
          .then((data) => {
            onLoadedFile(data)
            message.success('Изображение(-я) успешно загружен(-ы)')
          })
          .catch((err) => message.error('Произошла ошибка при загрузке файла'))
      } else if (e.type.indexOf('image/') !== -1) {
        const image = new Image()
        image.src = window.URL.createObjectURL(e)
        image.onload = () => {
          onDataLoaded({
            galleryImages: [
              {
                url: image.src,
                width: image.width,
                height: image.height,
              },
            ],
          })
        }
      }
    },
  }

  return (
    <StyledContainer>
      <Form
        form={form}
        name="horizontal_login"
        layout="inline"
        onFinish={onLoad}
      >
        <Form.Item
          name="url"
          rules={[
            {
              required: true,
              message: 'Поле URL не заполнено',
            },
          ]}
        >
          <Input
            suffix={
              <StyledUpload {...uploaderProps}>
                <UploadOutlined />
              </StyledUpload>
            }
            placeholder="URL"
          />
        </Form.Item>
        <Form.Item shouldUpdate={true}>
          {() => (
            <Button
              loading={dataLoading}
              type="primary"
              htmlType="submit"
              disabled={
                !form.isFieldsTouched(true) ||
                form.getFieldsError().filter(({ errors }) => errors.length)
                  .length
              }
            >
              Загрузить
            </Button>
          )}
        </Form.Item>
      </Form>
    </StyledContainer>
  )
}

ImageLoader.propTypes = {
  onDataLoaded: PropTypes.func.isRequired,
}

export default ImageLoader
