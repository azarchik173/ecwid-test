import React, { useState, useEffect } from 'react'
import styled from 'styled-components'
import PropTypes from 'prop-types'

const HEIGHT_COEFFICENT = 0.15

const StyledContainer = styled.div`
  display: flex;
  flex-flow: row wrap;
  jusitfy-content: center;
  align-items: center;
  padding: 10px;
`
const StyledImage = styled.div`
  ${({ url, width, height }) => {
    const proportion = height / width
    return `
        position: relative;
        flex: auto;
        min-width: calc(calc(320px * ${HEIGHT_COEFFICENT}) / ${proportion});
        width: calc(calc(100vw * ${HEIGHT_COEFFICENT}) / ${proportion});
        min-height: calc(320px * ${HEIGHT_COEFFICENT});
        height: calc(100vw * ${HEIGHT_COEFFICENT});
        max-height: calc(860px * ${HEIGHT_COEFFICENT});
        background: url(${url}) no-repeat;
        background-size: 100% 100%;
        margin: 3px 3px 0 0;
        flex-grow: 1;
    `
  }}
`
const Gallery = ({ images }) => {
  const [isLoaded, setLoaded] = useState(false)
  useEffect(() => {
    if (images.length) {
      setLoaded(true)
    }
  }, [images])
  return (
    <StyledContainer>
      {isLoaded &&
        images.map((image, idx) => (
          <StyledImage key={`${image.url}-${idx}`} {...image}></StyledImage>
        ))}
    </StyledContainer>
  )
}

Gallery.propTypes = {
  images: PropTypes.array.isRequired,
}
export default Gallery
