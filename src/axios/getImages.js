import axios from 'axios'

export const getImages = (url) => axios.get(url)
