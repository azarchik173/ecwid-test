import React, { useState } from 'react'
import styled from 'styled-components'

import ImageLoader from './components/ImageLoader'
import Gallery from './components/Gallery'

const StyledContainer = styled.div`
  margin: 0 auto;
  padding: 40px 0;
  min-width: 320px;
  max-width: 860px;
  overflow: overlay;
`

function App() {
  const [imageData, setImageData] = useState([])

  const fileLoaded = (arrayFromFile) => {
    setImageData((prevState) => [...prevState, ...arrayFromFile.galleryImages])
  }

  return (
    <StyledContainer>
      <ImageLoader onDataLoaded={fileLoaded} />
      <Gallery images={imageData} />
    </StyledContainer>
  )
}

export default App
