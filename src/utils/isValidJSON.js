export const isValidJSON = (json) => {
  try {
    JSON.parse(json)
  } catch (err) {
    return false
  }
  return true
}
